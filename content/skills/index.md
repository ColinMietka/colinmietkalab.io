---
title: "Skills"
description: "Technical Skills"
layout: "simple"
---

{{<lead>}}Programming Languages & Tools{{</lead>}}

{{<keywordList >}}
{{<keyword icon="python">}} Python {{</keyword >}}
{{<keyword icon="github">}} Github {{</keyword >}}
{{<keyword icon="gitlab">}} Gitlab {{</keyword >}}
{{<keyword icon="docker">}} Docker {{</keyword >}}
{{<keyword icon="jenkins">}} Jenkins {{</keyword >}}
{{</keywordList >}}

This is my main set of development skills. I am expert with python based data science libraries like pandas, scikit-learn, gensim, nltk, keras and torch. Every project I've been part of since 2018 involved at least a part of them. As a data-driven business consultant, I practiced a few data visualisation libraries like seaborn, plotly and dash but also the proprietary tool QlikSense©. I had the opportunity to develop in pyspark also for a few months. I often work with git as a version control system and I am also quite familiar with DevOps set of practices.


{{<lead>}}Web Development{{</lead>}}

{{<keywordList >}}
{{<keyword icon="html5">}} HTML5 {{</keyword >}}
{{<keyword icon="css3-alt">}} CSS3 {{</keyword >}}
{{<keyword icon="js">}} Javascript {{</keyword >}}
{{<keyword icon="php">}} PHP {{</keyword >}}
{{<keyword icon="bootstrap">}} Boostrap {{</keyword >}}
{{<keyword icon="wordpress">}} Wordpess {{</keyword >}}
{{</keywordList >}}

I recently put a lot of my free time learning some Web Developement skills. They were needed for a project of community website, [Les Voyageurs du Peloton](https://www.voyageursdupeloton.fr/), where people can share their cycle touring experiences.
That beeing said, I would consider myself a novice in this field.


{{<lead>}}OS & Others{{</lead>}}

{{<keywordList >}}
{{<keyword icon="linux">}} Linux {{</keyword >}}
{{<keyword icon="apple">}} MacOS {{</keyword >}}
{{<keyword icon="microsoft">}} Windows {{</keyword >}}
{{<keyword icon="google">}} Google {{</keyword >}}
{{</keywordList >}}


{{<lead>}}Languages{{</lead>}}

{{<keywordList >}}
{{<keyword icon="globe">}} French {{</keyword >}}
{{<keyword icon="globe">}} English {{</keyword >}}
{{<keyword icon="globe">}} Spanish {{</keyword >}}
{{<keyword icon="globe">}} Chinese {{</keyword >}}
{{</keywordList >}}

 French is my mother tongue and I'm perfectly able to work in English (TOEFL: 637/670). In fact, I already have, several times. I learned a lot of Spanish at school and went a semester abroad. I also know a little Chinese, just a little... 
