---
title: "Colin Mietka"
description: "Colin Mietka, Data & AI Engineer"
---

{{<typeit
    loop=true
    lifelike=true
    breakLines=false
    speed=80
>}}
Data & AI Engineer
Passionate about Data Science
Open Source Software Advocate
Always Looking for New Challenges
{{</typeit>}}
