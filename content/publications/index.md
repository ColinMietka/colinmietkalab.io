---
title: "Publications"
description: "Research papers I published along with colleagues"
layout: "simple"
---

- Modulated equations of Hamiltonian PDEs and dispersive shocks. S. Benzoni-Gavage, C. Mietka, and L.M. Rodrigues.<br>
Nonlinearity, Vol. 34 (2021), no. 1, p. 578-641.


- Stability of periodic waves in Hamiltonian PDEs of either long wave-length or small amplitude. S. Benzoni-Gavage, C. Mietka, and L.M. Rodrigues.<br>
Indiana University Mathematics Journal, Vol. 69 (2020), no. 2, p. 545-619.


- Ondes périodiques dans des systèmes d’ÉDP hamiltoniens. Stabilité, modulations et chocs dispersifs. C. Mietka - 2017. [Thesis Manuscript](manuscrit_mietka.pdf)


- On the well-posedness of a quasi-linear Korteweg-de Vries equation. C. Mietka.<br>
Annales Mathématiques Blaise Pascal, Tome 24 (2017) no. 1, pp. 83-114.


- Co-periodic stability of periodic waves in some Hamiltonian PDEs. S. Benzoni-Gavage, C. Mietka, L.M. Rodrigues.<br>
Nonlinearity, Vol. 29 (2016), no. 11, p. 3241-3308.

- Study of a depressurisation process at low Mach number in a nuclear reactor core. A. Bondesan, S. Dellacherie, H. Hivert, J. Jung, V. Lleras, C. Mietka and Y. Penel<br>
ESAIM: Proceedings and Surveys, EDP Sciences, 2016, CEMRACS 2015: Coupling multi-physics models involving fluids, 55, pp.41-60.


- Acoustic Propagation in a Vortical Homentropic Flow. J-F. Mercier, C. Mietka, F. Millot, V. Pagneux - 2014. [Hal Document](https://hal.inria.fr/hal-01663949/document)
