---
title: "Own your Data"
date: 2024-06-24
description: "A few inputs on how I started my homelab"
summary: "A few inputs on how I started my homelab"
tags: ["Data", "Homelab", "Privacy"]
---

{{<lead>}}
What can you do with the old computer you have in the closet ? I decided to install a Linux system and use it as a homelab. A background computer that can run a few services like serving media on your network, hosting your website, keep a copy of your data, and much more !
{{</lead>}}


## How I started my homelab

### What do I do with my old hardware ?
A good question right ? I bought a new computer before the old one turned useless, probably like many people do. Nobody had a use for it except me at the time so it slept in a closet for almost 2 years until I decided to revive it. 
My idea was to remove the old mac os system and replace it with a *new linux user* friendly distro for the rest of the family. I went for Linux Mint, an Ubuntu based distro with a good reputation among new users. And as an Ubuntu base, I was gonna be able to practice a few skills on server management, and maybe turn the old laptop into a media center. At least that's what I thought at start.

### Get started
As often for me, the journey was as important as the destination. My goal was not only to use the built server in the end, but also to learn how to put it in place and manage it. I found a few Youtube's channels to get ideas and good practices on how to build a first homelab. I can recommand [TechnoTim](https://www.youtube.com/@TechnoTim), [Wolfgang's Channel](https://www.youtube.com/@WolfgangsChannel) and [Christian Lempa](https://www.youtube.com/@christianlempa). The most useful for me has been the one of TechnoTim, acompagnied by his [documentation website](https://technotim.live/). You will find in here all kinds of tutorials that can help you understand what you need to know about hardware, networking, containers and how to configure them. In fact, after a few days researching on the subject, I found myself discovering a complete community of *Homelabers*. Everyone has it's own usecases and preferences but you learn a lot just by looking at what others do and publish on the web. 

{{<figure
    src="docker.png"
    alt="Docker Logo"
    caption="[Docker](https://www.docker.com/) is a great solution to run services as containers."
>}}

### Know what you want to do
Having a lot of options, and as much advice is great. But the drawback is that you need to focus on what you need and want to do. You will find on the internet that people do not stop at a simple computer to build homelabs sometimes. You can be overwhelmed by information on hardware, proxies, networks, and so on. People are building entire data centers in their garage, at a prohibitive cost obviously. In my case, I just want a few services: storing pictures and files, serving movies to other devices at home, maybe have my own gitlab instance to test things out. In short, I decided I was **not** gonna build a noisy and power hungry setup just for fun !

### What about cost ?
Another aspect to have in mind. You plan to have a computer running 24/7 at your house. What's the power bill behind ? Using an old computer, not made for this usecase is probably a bad idea. You can find low power consumptions PCs, or *mini PCs* maybe, but I find them to lack in power and storage. It's probably why people on the internet build entire networks for their homelab. They couple storage devices, NAS, networking devices like firewalls and VMs using *hypervisors* to run the containers. Well, for the moment, I only have the old laptop, so I power it on just when I need it. It will do for the time being but I plan to find a good setup, with enough power and storage to support my usecase (which is not too much hungry I guess) and with a low power consumption. The perfect setup do not exists probably, but I'll get as close as possible. But as I said, I don't want to build a data center at home, I'll stick with only one machine.

## My current setup

### The old laptop got a second life
As I said, I installed Linux Mint on my old Macbook Pro from 2013. It was declining more and more and needed to have a cleanup. I know Linux Mint is not a perfect *server* distro but it's not the main usecase of a laptop after all. Mint is easy to use and because of it's proximity to Ubuntu (on the system side of things) it's a close enough choice to get started. It has 8Go RAM and a 250Go SSD storage drive. Both felt kind of small, as you'll see bellow. But I have nothing to say about the distro itself. I found all application I needed and maintenance is much easier than on my Archlinux desktop. The Cinnamon desktop environement is good enough for the whole family usage, even if when I use it, I miss my KDE setup a lot ! But in the end, everything feels as if it was just a brand new laptop. Which, for a 10 year old device is really good. 

{{<figure
    src="nextcloud.png"
    alt="Nextcloud cloud platform logo"
    caption="[Nextcloud](hhttps://nextcloud.com/), the open-source, on-premises content collaboration platform"    
>}}


### Core services I wanted to have
The whole homelab experiment started for one particular service, and it is [Nextcloud](https://nextcloud.com/). Basically, it offers the same kind of services as the Google suite: calendar, file storage, parallel editing, pictures management, etc... but it's free and open source. I quickly faced challenges to put the necessary containers in place. I'm not too familiar with Docker and just have basic understanding of how to create containers and deploy them. So I decided to look at something simpler and also at a Docker utility called [Portainer](https://www.portainer.io/) to help me manage the containers. After spinning up Portainer, I started Looking at [Jellyfin](https://jellyfin.org/), a FOSS media solution. It was much easier to deal with and it felt good to have my first service running. 

{{<figure
    src="homepage.png"
    alt="Homepage Service example showing various services running"
    caption="[Homepage](https://gethomepage.dev/latest/) Service example showing various services running."
>}}


Of course, all of it was still running on local network, which is great but why not share the movies library with the rest of the family ? And the really technical part started. I'll give more details about it bellow but just have in mind that networking, IP adresses, DNS records, proxy servers are the basa of it all. You need to have at least a basic understanding of networking to have your homelab running safely from home and access services remotely. 

After Jellyfin, I came back to Nextcloud and had it working as well. It's a wonderfull solution that I use now to synchronize the pictures from family smartphones. It's easy to setup with their dedicated app on Android. I'd very much like to store a complete copy of the photos library but the laptop lacks storage. It will have to wait... I still have to explore the many possibilities with Nextcloud. It has a huge library of apps and plugins to help you do whatever you would want to do. I just tested a with things about synchronizing contacts and calendars between Android and my desktop Thunderbird client.

The also deployed my own [Gitlab](https://docs.gitlab.com/ee/install/docker.html) instance. I did not have much experience with the use of their runners and pipelines and wanted to test things out. Turns out that you can already build pipelines and use free runners directly with your free account on Gitlab.com, at least when you don't use too much compute time. But it was still a great experience to try deploying it and have access to the backend settings of such a tool.

### The technical part
Up until this point, everything was going smoothly. The only difficulty I faced was opening the ports of my router, setup the reverse proxy using [Traefik](https://doc.traefik.io/traefik/) and SSL certificates from [Cloudflare](https://www.cloudflare.com). So the networking part was a struggle. I had no issues regarding the server load until I added the Gitlab container. Suddenly, the old laptop's 8Go of RAM where looking too small. How did I notice ? First by the fan noise... And then I added a monitoring stack of containers to keep an eye on the server load from a distance. For the moment I use a combination of [Prometheus](https://prometheus.io), [Loki](https://grafana.com/oss/loki/?pg=logs&plcmt=options) and [Grafana](https://grafana.com/). When combined, you are able to get a lot of monitoring data from your machine, and nice dashboards to analyze what's happening. 

{{<figure
    src="grafana_dashboard.png"
    alt="An example of Grafana Dashboard"
    caption="An example of Grafana dashboard [Node Exporter](https://grafana.com/grafana/dashboards/1860-node-exporter-full/)."
>}}

## Future plans

### Dedicated PC build
I mentioned it earlier but I reached the limits of my laptop with just a few services running on a dozen containers. It's no surprise in reality because it's old hardware, not ment for it, and running on a system also not optimized for this usecase. So my next goal is to build a small case PC with lots of storage and RAM to have at home running 24/7. When I find the perfect setup, I'll probably write a new post to describe it !

### More power, more services ?
Do I need more services, no. Do I want to explore and test new things, probably yes. I don't really know what I want to try next. I saw a lot of discussions about the enrichment of media libraries using the [Servarr](https://wiki.servarr.com/) stack, web server using [Nginx](https://nginx.org) to host my own website maybe. Who knows !

## Why should you care about owning your data ?

### Privacy
Yes, why should you even consider having a second computer at home, right ? And why bother with services, programs and maintenance whereas for a few coins per month, everything can be stored in the cloud, by a giant tech company that saves all your data for you... You see the point coming, even if it's not the money (when you have thousands of pictures, storage price might become a problem), the problem is that you give up your privacy to these platforms. Not all of them are collecting data of course, I imagine a company like [Proton](https://proton.me/) offers their storage and services with privacy features for example but the best way to keep you things yours is just to keep it with you.

### Have better control on what you own
Do you know how many online accounts you have, where are all the passwords. Where did you store your pictures over the years, Apple's ICloud or Google Photos maybe. Some may be on social media now and it's the only copy  you have left. Do you have also a music library ? And what about administrative files and folers you created over the years ? My point is, keeping your data at home will probably help you keep track of what you have and keep it safe.

### Backups
I did not talk too much about backup. Obviously you have to make them, and the homelab can host a copy of your data. But as always, don't put everything in the same place. It is said that a good backup startegy is to have 3 copies of your data, in at least 2 places, with 1 which is not your home. The famous *3-2-1* rule to save all your data.
