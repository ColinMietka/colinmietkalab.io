---
title: "How I discovered Static Site Generators"
date: 2024-04-22
description: "How I discovered Static Site Generators and what I intend to do with it !"
summary: "How I discovered Static Site Generators, chose the one I'm using right now and deployed on gitlab pages my first static site !"
tags: ["SSG", "Hugo", "Blog"]
---

{{<lead>}}
I'm not a web developper, just an anthusiast with some development related skills. And using SSGs is just a really fast, simple and enjoyable way to publish a website with content.
{{</lead>}}

## My Experience with personnal websites and HTML

### Simple HTML Page
My take on how to how a website has evolved a lot during the past years. I started with simple html pages with copy-pasted css files. It was almost 10 years ago now. I needed to create a simple webpage to be used as my academic profile. It was just a few html files, nothing fancy.

### Bootstrap themes
Over the years, I improved a lot, especially on the PHP, mysql side for projects related to [Les Voyageurs du Peloton](https://www.voyageursdupeloton.fr/). I used Wordpress for a while but realized quickly that it didn't suit my needs (at least at the time). So in 2020, I decided to dive into bootsrap and made my own PHP website. As I said, I'm not a web developper and I imagine what I did, in all humility, is probably not made to last... But it has the great feature of still beeing running today. In the meantime, having access to this new piece of technology made me realize that my old website needed a revamp. I searched for a dedicated [boostrap template](https://github.com/startbootstrap/startbootstrap-resume) and updated it. Again, a simple copy-paste from (this time for sure open source) template and I had a new version running. My customization skills were still very poor, the css part is clearly not my strong suit and I re-used the template almost entirely. 

## And then, I Discovered SSG existed

### What are SSGs and how many are there ?

{{<figure
    src="ssgs_pages.png"
    alt="SSGs Pages"
    caption="Static Site Generators from [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)"
>}}

And now, we are in 2024 and technology don't stop evolving. I hearded about Static Sites Generators beeing the go to in terms of blogs. Anything that didn't need a database in fact. It seemed to me that there were a lot of options, all written in their own language. From what I understood, their goal is pretty much the same and it's to ease the development of content related websites. It is particularly made for blogs or documentation websites. They aim at serving the fastest way possible each page. And about these pages: html is gone ! You write in markdown. All is taken care of for you by the server as some kind of all in one solution for the development. So why not try it ? It could be a good way to learn a new framework and again, my old website deserved a paint job.

### How I chose the one used for this website
As always when wanting to try something new, I searched on the internet what was the most popular framework. The idea for me is to try and learn one of them, not all. And in doing so, I try to find one that is representative or at least that I imagine will still be there in  a few years. I narrowed it down to three pretty quickly:
- [Hugo](https://gohugo.io/)
- [Jekyll](https://jekyllrb.com/)
- [Astro](https://astro.build/)

Astro looked like the newcomer on the scene with huge performances and a lot of promisses. It looked also not as used as the other two (in early 2024) and I was afraid of it's apparent complexity. 
Regarding Jekyll, the solution developper by Github, I found a lot of suitable templates but I must admit, the ones I found on the Hugo side were better. I also wanted to avoid going down the Github route. Their solution is completely fine, it's just a matter of preferences. 

{{<figure
    src="featured.jpg"
    alt="Blowfish Theme for Hugo"
    caption="[Blowfish](https://blowfish.page/) Theme for Hugo."
>}}

Now, with Hugo templates were legion. It's the most used framework at the time I'm writting this. It's written in Go, it's fast and the command line utility looked easy enough to use for me. After spending a few hours searching for a theme or template on their website, I settled down on the maginificent [Blowfish](https://blowfish.page/) ! It's full of features and was quite easy to use as you'll see.


## Creating the site and Deploying it

### Follow the theme guidelines
Ok, I say it was easy and I lied, sort of. My first few attempts were failure, but not complete ones. I managed to create websites, import templates as modules, or as git submodules and visualize my site locally. The difficult part came after, when trying to deploy as gitlab pages. I'll explain why later. For now, I would say that the [documentation](https://blowfish.page/docs/) for Blowfish is really good and I had no trouble following it to create my first site. You will find in here everything that you need to create content quickly. I had my first version running in just a few hours, even without knowing how hugo worked at start. For this template, you have layout options for the langing page and for the content pages. A lot of features are taken care of whithout you knowing. For example, you can see the word count and reading time at the start of each post and for this I did nothing except uncommenting a few lines in a config file. In fact, most of the templates work like this behind the scenes. Just a few config files for you, and a lot of power available with it. 

Another cool feature I used is [shortcodes](https://blowfish.page/docs/shortcodes/). They are basically javascript functions to allow a few dynamic behaviors in your site. There are quite a few available in Blowfish. I used the timeline one for the resume sections, and the typeit one is nice looking !

### Use Gitlab Pages
Another cool thing about these SSGs is that the two giants Github and Gitlab are able to host your website for free. As static sites managed in repositories, they offer a **pages** solution to display your html content. As already mentionned, I prefer to use Gitlab nowadays and their [Pages](https://docs.gitlab.com/ee/user/project/pages/) documentation was quite helpful along with the one given on directly by [Hugo](https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/). The idea is basically to use the CICD capabilities of Gitlab (by creating a job pipeline in a yaml file) to publish the website on a predefined domain. This domain is by default linked to your Gitlab username but you can modify it. I chose to leave it as is and to also deploy the site with my previous web hosting service.

{{<mermaid>}}
graph LR;
A[Write your site]-->B[Commit & Push]
B-->C[Pages CI Job Running];
C-->D[Pages Published]
{{</mermaid>}}

That was the part that gave me some trouble. I did not manage to deploy a Hugo site built with modules (theme modules for example). The only way I succeeded was using git submodules for the template. It's not that limiting but still, it was frustrating to be set back.

### Or to any web hosting service
Nothing easier than publish your hugo site to any web hosting service. For example, mine give me access throught ftp to drop my html file into their server. In this case, the only thing you have to do is to copy the content of the **public** folder, created when you bluid your Hugo site. It is the folder containing all the markup html files for your website. In just a few minutes, I had two domains running my website.

## Wrap Up
Frankly, I love how this SSG work right now. I'm able to publish this page just by typing text basically, not using any html and that's a relief. In fact, it looked so simple, it made me start a blog. We'll see if I keep up and still publish in a few months but I felt there was no better time than with this site setup. 
