---
title: "DeGoogle your phone"
date: 2024-10-10
description: "Google or Apple phone ? What if another path was possible ?"
summary: "My journey to e/OS/, an open-source deGoogled Andoid ROM that focus on Privacy."
tags: ["Phone", "Android", "Privacy", "e/OS/ ", "Murena", "ROM"]
---

{{<lead>}}
With nowadays smartphone, you can do anything. And the two main systems reign supreme on the market. But if you value your privacy, another path is possible. It is called e/OS/. It's open source, it's free, and it doesn't spy on you. 
Let's see what it is and how to finally switch your phone to it !
{{</lead>}}

## Should you care about the system on your phone ?

### Google or Apple, pick your poison.
It's been at least ten years now that mobile phones are completely part of our daily life. We have them within reach at all times. We use them as phone for sure, but also to access the web, organise our life, share on social media, ...
And for the last ten years at least, the market has been dominated by two giants: Google and Apple. They developped the base systems on wich our devices run, namely Android and iOS. And of course, 
they use now this dominant position to slowly shape our day to day devices into some sort of surveillance systems. They extract and share our personnal data with third parties, beeing public or private, for money. 
In the name of convenience, they predict and shape our behaviors.
But not all is lost, it turns out that the base Android sytem is open-source. It's called Android Open Source Project (AOSP) and this opens the door to other possible operating systems for our phones.

{{<figure
    src="LineageOS.svg"
    alt="LineageOS Logo"
    caption="[LineageOS](https://lineageos.org/) is one of the most popular free Android ROM."
>}}

### What are Custom ROMs ?
An Android ROM (Read-Only Memory) is an alternative version of Android. It can be built in top of the open source base project. There are a lot a different ROMs available, and you can find a list, 
probably non-exhaustive on [Wikipedia](https://en.wikipedia.org/wiki/List_of_custom_Android_distributions). Everybody could build he's own system and customize it as he needs. 
Then, some of the developpers decided to release their version and make it available for others. The most famous are [LineageOS](https://lineageos.org/), [GrapheneOS](https://grapheneos.org/) and [e/OS/](https://e.foundation/). 
Of course there are many more options but when you first search the internet to learn more about them, they are the 3 most discussed options.

### e/OS/, an open-source and privacy respecting ecosystem.
I had a closer look at e/OS/. They propose not only the base system but also a complete ecosystem of privacy respecting applications that completelly replaces the Google suite that you have on your phone. 
It is based on LineageOS which is already a pretty complete system, and they add applications on top of it. You can find all the details you need on their [e/OS/ page](https://e.foundation/e-os/). 

One of the main downside with the custom ROMs, that we will discuss in a moment, is the device compatibility It looks like e/OS/ has something for a lot of them, beeing officially supported by the foundation 
or just by the community. Of course, all they do is open-source, and if you can/want to contribute, you can find everything on their [gitlab](https://gitlab.e.foundation/e). 
You can also learn more about their mission and goals in their [Manifesto](https://e.foundation/about-e/).

{{<figure
    src="eos_website.png"
    alt="A screenshot from e/OS/ website"
    caption="Screenshot from [e/OS/](https://e.foundation/) website."
>}}

## Can I switch my phone to e/OS/ ?

### Supported devices
Now that I got you attracted with all the good promises of a system like e/OS/, it's time to come back to reality. Installing a custom software on your current Android phone is not that easy to do. 
In general, the devices supported are well documented and you can instructions in the documentation. 
At the time I write these lines, e/OS/ supports almost 200 devices, but the list of officialy supported (by the foundation) is much shorter. 
For a complete list of supported devices, go check the [device list](https://doc.e.foundation/devices). 
In short, the support won't be the same for all devices. For the majority of them, the maintenance is done by community members. 
It means installing and updating may not be easy, and some of the functionalities may be missing, causing some apps to be unusable (typically banking apps). 
For the officially supported devices though, you get a simple install, updates over-the-air (OTA) and all apps working, exaclty as you would expect from a stock Google Android device. 
So when trying to see if you could get a shot at e/OS/ or any other ROM, definitely take a look at these device lists !

### Installation method
I'll present the two main methods of installation for e/OS/ as I tried both of them for two different devices. Of course, these methods vary for other systems so go check out the dedicated documentation. 
Regarding e/OS/, you basically have two methods, the manual one with the command line (different for each device), and their [easy-installer](https://doc.e.foundation/easy-installer) application. 
he easy-installer supports only a few devices for now, but if you're not confortable with the command line, it's a life-saver ! 
If you're familiar with the command line and not too afraid of breaking your phone, you may try the manual installation. 
It certainly gives you the feeling you've learned something when you reboot the phone and the e/OS/ welcome screen appears. 

There is a third option, which is building your own ROM from source. It's the most complicated option, too complicated for me, but it probably offers a lot of customizations that you way want to test. 
This is also probably the way to try and port the system to a new device. I would advise this method only for actual developpers who know what they're doing... 

### My choices and experience with e/OS/
I had no experience with these custom Android ROMs before and e/OS/ is the first one I tested. I compared LineageOS, e/OS/ and other options without Android 
like [postmarketOS](https://postmarketos.org/) that I will probably try in the future. 
I went for e/OS/ just because I found some reviews (that I can't find anymore) that were really great. It also matched better with the devices I had available at the time. 
In the end, I feel that it just comes to what OS my device can supports. Because at least for a first try, I didn't want to buy a new, or even refurbished device. 

I tested a first install on an *old* Samsung A20e (it was sitting in a drawer) wich is only community supported by e/OS/. I found a really good [post](https://community.e.foundation/t/howto-an-unified-install-guide-project-for-e/36234) 
on the community forum that explains the general method to install e/OS/ on a Samsung device. I had to download a ROM created by a community called [Eureka](https://eurekadevelopment.github.io/) and followed the instruction to 
unlock the phone's bootloader, install TWRP, the recovery tool that is used to install the actual e/OS/ system, and then install e/OS/ itself. 

Technically, at some point while I was following the installation guide, I felt like I destroyed my phone. But in the end, I managed to make it reboot into e/OS/. What a relief ! 
I used it a couple of weeks, and I could tell it has a good potential but some things were not working at all. 
First, almost every appplications were working, even the most demanding in security. Banking apps were completly ok, but the main issue was with the bootloader unlocking. 

Because you need to unlock it to install e/OS/, some applications consider the phone *insecure*. I discovered later that you can re-lock the bootloader, but not on all devices ! 
After that, installing application worked, it was not perfect because the incognito mode on the e/OS/ store called *App Lounge* was not working well. 
I still managed to run all I wanted via [Aurora](https://auroraoss.com/) or [F-Droid](https://f-droid.org/). As a community version, it was a bit outdated with only *Android 12* if I remember well and older security updates. 
Sometimes, I think I may be due to some mistakes in the installation process but who knows.

The experience was good enough for me to make the switch. But this time, I decided to search for a well supported device and buy a refurbished one. 
I went first to the e/OS/ foundation (Murena) [store](https://murena.com/products/smartphones/). They sell and ship smatphone with preinstalled e/OS/. 
I also checked the officially supported devices in the [device list](https://doc.e.foundation/devices) and searched for the good smartphone for me. Of course, this is just a personnal choice, but even if 
I considered a Fairphone with all the good will it has to sell, I thought the device not suiting my needs (which is battery life mostly). Ironiclly, it seems that one of the most supported devices 
(not only for e/OS/ but also for other systems) is the Google Pixel 5. What a joke ! So I bought one and this time I used the easy-installer. It was *flowless*.

## What to expect after installing

{{<figure
    src="eos_apps.png"
    alt="A screenshot from e/OS/ website with home screen and applications"
    caption="Screenshot from [e/OS/](https://e.foundation/e-os/) website, with home screen and applications. "
>}}

### It's just working as any other Andoid phone
It's now been a few month using e/OS/ daily. The install process went super well and I have exaclty all I needed. It's just working as my previous Android phone except I don't have the preinstalled applications, either by Google or
the manufacturer (think about all the Samsung Apps nobody uses and that cannot be uninstalled). Banking Apps work perfectly, NFC, Bluetooth, Wifi, Health care, everything. All without any Google account, no adds, no trackers, ... 
It's trully my phone.

### SafetyNet and bootloader relocking
As I said before, bootloader relocking was a problem in my first attempt. Without it, some of the apps (Doctolib for health care for example) were just not working. 
With the good device and the easy-installer, I relocked the bootloader after installation and never had any issue. Something that I didn't encounter but can be blocking for you is the SafetyNet. Even if I don't understand the details of it, 
it is mostly needed for banking applications so make sure that you choose a device that supports it. For me, with the official Pixel 5 *redfin*, all good !

### Try it out !
You have an old phone in the closet, I know it ! Just don't be afraid to break it, and try e/OS/ or anything else you prefer. I'm sure you will be tempted, as I was, to just change the way you live with your phone.
