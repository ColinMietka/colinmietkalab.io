---
title: "My Linux Journey"
date: 2024-05-29
description: "Everything about my though process while switching to linux desktop."
summary: "A few years ago, I decided to leave macOS and Windows and here is how and why."
tags: ["Linux", "OS", "FOSS"]
---

{{<lead>}}
I have used computers all my life, beeing for work or pleasure like many of my generation. I used the 3 major OS on the market and I have to say that the best one for me is Linux and here is why I switched away from the others.
{{</lead>}}

## A bit of History

### Early days with Windows
I grew up with the computer of my father. At the time obviously, the main operating system out there was Windows by far. I learned to use a computer on Windows 95 and as I got older, used Windows systems until I think Windows Vista for personnal use. I stopped using Windows on my main machine early 2013. Was I disapointed with the OS? Probably. But the main reason I decided to leave Windows was the hardware. As a student, I bought two laptops (cheap ones) and the harware failed on me badly. The choices I made were probably not the best but I didn't have the money to buy a really good computer with great battery life, computing and gaming capabilities, etc... 

### Linux Dual Boot
I remember giving a try to linux using Dual Boot. A few friends of mine were just toying with the command line on Ubuntu. So I practiced a little and demystified the terminal utility as one can say. But it never got past a few programs and usecases, it was just for fun. 

### When Apple took over
Early 2013 then, I decided to take advantage of the discount my school granted to buy one of newly designed MacBook Pro. The promise for me was a better hardware and the macOS desktop features. I was impressed by the os capabilities and design as it was way better (in my humble opinion) than the Windows one. For the harware, I decided to pay a little extra money to buy the latest SSD devices and have harware that will last as long as possible. My idea was, that if you buy hardware that is already a few years old, it will be outdated quicker. I think it went well, because we are in 2024 and this MacBook Pro is still working! I did everything with it during almost 10 years. All the usual web browsing, some gaming (obviously not the latest games but still), I even used it during my PhD as my main machine to perform computations, and write my manuscript. In the end, apart from a small issue with the speakers, it was perfectly working in 2022 except Apple didn't ship updates for it anymore...

## I felt a change was needed !

### Apple's update policy
The OS update issue didn't arise in late 2022, it was much earlier. When trying to update, the system would have this warning saying that the hardware is not supported anymore (or at least badly) or even that the OS itself will be taking too much storage space for it to be installed on my machine. I was honestly angry at Apple for that. The computer is perfectly fine but as time goes by the system would be more and more oudated and unusable. At first, I didn't care but at some point some of the softwares were not even working. For example, Safari would refuse to display some websites because it's version was too old. Of course, You can always change the softwares you use to have ones that are actually working but it did not felt right to me. I was just pushed away. 

### Windows is still a nogo
Because I also wanted a new PC to install some recent games, why not come back to Windows? That one is easy, I have the newest Windows 11 version at work, and it's a nightmare. No way I use this if I have a choice.

### What about those new M1 iMacs ? 
I took a look at the new M1 [iMacs](https://www.apple.com/imac/) that were just realsed at the time. The looked like powerfull machines, clearly easier to integrate in your home interior than a clunky tower and screen. Unfortunaltelly, they will have, in time, the anticipated end of life decided in an office far away by the giant company making them. I was seriously considering it but I also wanted to see what Linux had to offer, 10 years after our last encounter. I found out that it was as powerful as ever, could even run games now thanks to [Valve's Proton](https://github.com/ValveSoftware/Proton) contributions and that there was basically no use case left out now. A friend of mine was even installing Linux Mint on his PC and all that was enough to finally attract me.


## Linux it is then, but how ?

### Searching for advice
A good part of the decision was made because of the content of good quality I found about Linux, distributions, open source softwares, etc... A few Youtube content creators cought my attention rapidly, namely Nick from [The Linux Experiment](https://www.youtube.com/@TheLinuxEXP) and Jay from [Learn Linux TV](https://www.youtube.com/@LearnLinuxTV). Both of them, I still watch two years later. You can find on these channels everything you need to know about the new system you are installing. How to install it, what to expect, what will work and what may not work out of the box, you name it. They have a variety of subject that they cover and update regularly so it's in my opinion a pretty good start.

{{<figure
    src="distros.png"
    alt="Distributions Icons"
    caption="Icon set made by [Walruz](https://www.reddit.com/user/walrusz/)"
>}}

### Distributions
Here is the famous question: which distribution will you choose for your system? When choosing a distribution, from what I understood then, you gain access to a pool of **applications** and a given **Desktop Environment**. I know it's a simplified view of it but when you're a beginner, that is what it looks like. The number of distributions is overwhelming, but you can still see through their lineage. Roughly, you have *Debian* children, with after that their *Ubuntu* children (which are many) and you have also *Arch* children. The main difference beeing the way updates are handled. Arch children will have a short cycle, they are called *rolling release*, whereas Debian like have bigger chunks of upgrades. You then have to weight the fact that you want up to date software and dealing with regular updates that can be disrupting your system. In short: stability VS availability. 

Second main topic when choosing a distribution is the Desktop Environment it is shipping with. The two main ones are *Gnome* and *KDE*. Again, you have pros and cons for both of them but it seemed clear to me that the way they were developped is quite different. Gnome has it's own set of apps and a clearly defined aspect for the desktop. KDE on the other side has also it's apps but regarding the aspect, you have a lot of customizations available. I read also that it is said to be less stable than Gnome but beeing able to customize anything on your desktop is attracting.

One thing to keep also in mind is that depending on the distribution chosen, some software may be difficult to access. It is less and less true with the rise of the Flatpak format but it can still be an issue. 
If you want to have a look at the distros available, everything is on [Distrowatch](https://distrowatch.com/).

### My first choice and experiences
I finally bought a computer that I configured myself on [Top Achat](https://www.topachat.com/accueil/index.php). It's a french shop and I mention them here because I had a few discussions with their customer support and they were perfect. For the distribution, I remembered friends talking about Archlinux as if it was the only distribution worth it in this world but I thought it was too complicated for me at start. I wanted a rolling release but I needed to learn how things work first. And so I came to [Manjaro](https://manjaro.org/). It's an Arch derivative, with 3 options for the Desktop Environment. I chose KDE for it's customizations capabilities and I never regreted it! I still use KDE now and I find it to be very stable for my day to day use. As an Arch derivative, Manjaro has a short release cycle, but keeps back a few updates to be able to test for stability before making it available to the public. I thought it was a good compromise. 

Looking back at this choice, I think maybe [EndeavourOS](https://endeavouros.com/) would be a better starting point, it has the same Arch base but has a better reputation as a community than Manjaro.

## Two years later, what has changed.

### I gained experience and confidence
I used Manjaro for a complete year. I never had any major issue with the system, except once when after an update, I got a black screen. I managed to reinstall the Nvidia drivers that I needed using only the command line and fixed the problem myself just by looking for the solution on the internet. It felt so good to be able to do that, even if I thought for a minute that I would have to start over and reinstall everything! I installed Steam pretty easily and could play as much games as I want. Valve is making it easy for Linux users, you just need to check if the game you want to buy is well supported by proton on the [Proton DB](https://www.protondb.com/) website and you're good to go in my experience. 

I could do everything I needed with my Manjaro system, all the usual browsing, administrative tasks (I don't have a printer so I can't tell if that would work, but signing PDFs was easy), gaming, coding, etc...I was enjoying the **KDE Plasma** desktop, tweaking it to my liking and spending probably too much time customizing things using the command line and config files.

I loved learning how my system is working and I grew familiar with the command line utilities during this first year. And then I felt ready for the next step: installing **Archlinux**.

### Changing the distro
Before I continue, I must say that I get the whole "I use Arch btw" thing. Because the system you're trying to install is kept at bare minimum, without any graphical interface to work with at start, it feels like a challenge. Keeping it well maintained can also be if you're not carefull. But nowadays, with the immense quality of the [Archwiki](https://wiki.archlinux.org/), the installation is accessible and maintaining the system is not that hard. 

{{<figure
    src="arch.png"
    alt="Archlinux Logo"
    caption="The official Archlinux logo from [archlinux.org](https://archlinux.org/)"
>}}

Nevertheless, it is still a challenge when you install it for the first time and you don't know all about what is going on behind the scene. So I practiced the installation process on VMs, and failed a few times. Forgot to install bootloader, can't boot. Forgot the wireless network interface, no cable, no internet at reboot. It has been difficult but I finally managed to install Arch on two different VMs and then went for it for real. I did a backup of everything (you should too, always) and it went all smoothly! I'm writing these lines from this same Arch install, still working after more than a year. 

What I want to say is that it is rewarding. You have control over everything on your computer, you have the choice for every little piece of software you want to install. Nothing has been chosen for you befohand, or almost nothing, just the vitals. 

### What's next ?
I am at home with Arch on my main machine now. Everything is in place and I have no trouble in my day to day use or even for mainenance. I would still like to try other distros, and for that I erased macOS from my old laptop. It was not working so well anymore anyway. I first installed **Linux Mint**. It is an easy to use distro for the rest of the family that may use it. It's also definitely easier to maintain and won't suck up as much time as Arch. Maybe I'll switch again on this machine. I heard a great many things about **Fedora** and may give it a try. On the other hand, an Ubuntu based distro like Mint can be a way to get familiar with this very big family of distributions. After all, it's one of the main system running the world's servers.