---
title: "Interests"
description: "All about what I like to do"
layout: "simple"
---

Apart from working hours, I am a proud father. I am also president of the non-profit organization [Les Voyageurs du Peloton](https://www.voyageursdupeloton.fr/) for the development of cycle touring.

During the warm season, I enjoy hiking and climbing. I regularly practice running, swimming and cycling, altough never at the same time.

I am particularly found of discovering new cultures and landscapdes. I've been to a few foreign countries, Ireland, Spain, China, Vietnam, Mongolia, Mauritania, Tunisia, and I seriously plan on expanding the list.

When forced indoors, I follow a number of sci-fi and fantasy genre movies and shows, play a few video games and spend the rest my free time exploring the latest technology advancements in the data science world.