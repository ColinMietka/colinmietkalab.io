---
title: "Experience"
description: "All about work experiences"
layout: "simple"
---

{{<timeline>}}

{{<timelineItem 
    icon="work" 
    header="Data & AI Engineer"
    subheader="Capgemini Engineering - R&D, Toulouse"
    badge="November 2021 - Present"
>}}

{{<lead>}}
Technical leader of a development team in charge of ingestion pipelines in Palantir Foundry platform.
{{</lead>}}

Creation of pipelines between data lakes.</br>
Responsable of the respect of governance and cybersecurity rules.</br>
Technical support on pyspark developments.</br>
Agile Scrum master role.</br>

{{<lead>}}
Development of an open source simulation platform for multidisciplinary analysis and optimisation.
{{</lead>}}

Integrated Assessment Modelling in python. Global climate, economics and energy models.</br>
Model Based System Engineering, trades between Airbus future aircraft design.</br>
Optimisation algorithms and numerical analysis.</br>
Agile Scrum and DevOps practicies.</br>

{{</timelineItem>}} 


{{<timelineItem 
    icon="work" 
    header="Data-driven business consultant"
    subheader="Alten, Toulouse"
    badge="February 2018 - October 2021"
>}}

{{<lead>}}
Data analysis, statistics, machine learning and data visualisation.
{{</lead>}}

Business needs understanding and qualification.</br>
Data cleaning, enrichment, statistical analysis.</br>
Classification algorithms in Python and Pyspark, Natural Language Processings.</br>
Agile Scrum project management, support to recruitment and marketing process.</br>

{{</timelineItem>}} 

{{<timelineItem 
    icon="graduation-cap" 
    header="Researcher"
    subheader="Institut Camille Jordan, Lyon"
    badge="February 2014 - March 2017"
>}}

{{<lead>}}
Mathematical and numerical analysis of stability properties of periodic waves solutions of Hamiltonian systems.
{{</lead>}}

Analysis of partial differential equations.</br>
Development of a complex numerical method in Matlab, scientific calculation, numerical analysis, schemes for PDEs.</br>
Communication at scientific colloquium and training programs (CEMRACS 2015, SEME).</br>

{{</timelineItem>}}

{{<timelineItem 
    icon="graduation-cap" 
    header="Teacher"
    subheader="INSA Lyon"
    badge="September 2014 - June 2016"
>}}

Classes, exercices and practical work on numerical analysis for engineering students with Matlab.

{{</timelineItem>}}


{{<timelineItem 
    icon="graduation-cap" 
    header="Research Engineer"
    subheader="EADS Innovation Works - CERFACS, Toulouse"
    badge="March 2013 - January 2014"
>}}

{{<lead>}}
Mathematical analysis and development of numerical methods for the computation of turbo-reactors acoustic fields in a flow.
{{</lead>}}

Study of acoustic propagation models and their numerical simulation.</br>
Development of a finite element method code with Fortran.</br>

{{</timelineItem>}}

{{</timeline>}}

