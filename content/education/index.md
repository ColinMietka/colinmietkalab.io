---
title: "Education"
description: "All about dipmoma"
layout: "simple"
---

{{<timeline>}}

{{<timelineItem 
    icon="graduation-cap" 
    header="Institut Camille Jordan - Université Lyon 1"
    subheader="Doctorate in Mathematics"
    badge="February 2014 - March 2017"
>}}

Periodic waves in Hamiltonian systems: stability, modulations and dispersive shocks.</br>
Advisors: S. Benzoni-Gavage and L.M. Rodrigues.

{{</timelineItem>}} 


{{<timelineItem 
    icon="graduation-cap" 
    header="École Centrale Lyon"
    subheader="Engineering Degree" 
    badge="September 2010 - June 2013"
>}}

General Engineering training with R&D Specialisation</br>
Majors: mathematics and risks management, partial differential equations, acoustics.

{{</timelineItem>}} 

{{<timelineItem 
    icon="graduation-cap" 
    header="École Normale Supérieure de Lyon"
    subheader="Master degree MAIM" 
    badge="September 2012 - June 2013"
>}}

Mathematics and Applications, Advanced Mathematics and Mathematical Engineering.</br>
Majors: partial differential equations, kinetic theory and shape optimisation.

{{</timelineItem>}}

{{<timelineItem 
    icon="graduation-cap" 
    header="UPC Barcelona Tech"
    subheader="Exchange program ERASMUS" 
    badge="February 2012 - June 2012"
>}}

Classes in two schools of Universidad Politécnica de Cataluña BarcelonaTech.</br>
FME : Faculdad de Matemáticas y Estadistica.</br>
ETSEIB : Escuela Técnica Superior de Ingeniería de Barcelona.
{{</timelineItem>}} 

{{</timeline>}}

